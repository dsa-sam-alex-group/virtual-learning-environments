import ballerina/http;
import ballerina/uuid;
import ballerina/io;

# API description : This is a virtual learning environment API that allows leaners to access their profile
# modifyind them and access learning material with differents system and applications, this is gonna be 
# an open API meaning there will not be a key to access it. it is accessible through the url with any 
# machines or system 

isolated  final json Materials = 
{
    "course": "Distributed Systems Applications", 
        "learning_objects": {
            "required": {
                "audio": [
                    {
                        "name": "gRPC", 
                        "description": "performance Remote Procedure Call (RPC) framework that can run in any environment.", 
                        "difficulty": "Easy"
                    }
                ], 
                "text": [
                    {}
                ]
            }, 
            "suggested": {
                "video": [], 
                "audio": []
            }
        }
};

type Subjects record {|
    string course;
    string score;
|};

type Learner record {|
    string username;
    string lastname;
    string firstname;
    string[] preferred_formats;
    Subjects[] past_subjects;
|};

type LearningMaterials record {|
    string course;
    LearningObjects learning_objects;
|};

type LearningObjects record {|
    any required;
    any suggested;
|};

type Details record {|
    string name;
    string Description;
    string difficulty;
|};


isolated map<json> Learners = {};

isolated function getLearnerById(string Id) returns json {
    lock {
        return Learners[Id].clone();
    }
}

isolated function getAllLearner() returns json {
    lock {
        return Learners.clone();
    }
}

isolated function createLearner(string Id, json learnerReq) {
    lock {
        Learners[Id] = learnerReq.clone();
    }
}

isolated function UpdateLearner(string Id, json learnerReq) {
    lock {
        Learners[Id] = learnerReq.clone();
    }
}

isolated function getLearnerMaterials() returns json {
    lock {
        return Materials.clone();
    }
}

service /learner on new http:Listener(9090) {

    resource function get findLearnerById(http:Caller caller, http:Request req, string Id) {
        json payload = getLearnerById(Id);
        http:Response response = new;
        if (payload == null) {
            payload = "Learner : " + Id + " cannot be found.";
        }

        response.setJsonPayload(payload);

        var result = caller->respond(response);
        if (result is error) {
            io:println("Error sending response", result);
        }
    }

    resource function get findAllLearner(http:Caller caller, http:Request req, string Id) {
        json? payload = getAllLearner();
        http:Response response = new;
        if (payload == null) {
            payload = "No Learners found";
        }

        response.setJsonPayload(payload);

        var result = caller->respond(response);
        if (result is error) {
            io:println("Error sending response", result);
        }
    }

    @http:ResourceConfig {
        consumes: ["application/json"]
    }
    resource function post create(http:Caller caller, http:Request request) {
        http:Response response = new;
        var learnerReq = request.getJsonPayload();

        if (learnerReq is json) {

            string Id = uuid:createType1AsString();
            createLearner(Id, learnerReq);

            json payload = {status: "Learner Created.", Id: Id};
            response.setJsonPayload(payload);

            response.statusCode = 201;

            var result = caller->respond(response);
            if (result is error) {
                io:println("Error in responding: ", result);
            }

        } else {

            response.statusCode = 400;
            response.setPayload("Invalid payload received");
            var result = caller->respond(response);
            if (result is error) {
                io:println("Error in responding: ", result);
            }
        }

    }

    resource function put update(http:Caller caller, http:Request request, string Id) returns error? {
        var updatedLearner = request.getJsonPayload();
        http:Response response = new;

        if (updatedLearner is json) {

            json existingLearner = getLearnerById(Id);
            Learner CastUpdateLearner = check updatedLearner.cloneWithType(Learner);
            Learner CastExistLearner = check getLearnerById(Id).cloneWithType(Learner);

            if (existingLearner != null) {
                CastExistLearner.username = CastUpdateLearner.username;
                CastExistLearner.lastname = CastUpdateLearner.lastname;
                CastExistLearner.firstname = CastUpdateLearner.firstname;
                CastExistLearner.preferred_formats = CastUpdateLearner.preferred_formats;
                CastExistLearner.past_subjects = CastUpdateLearner.past_subjects;
                UpdateLearner(Id, CastExistLearner);
            } else {
                existingLearner = "Learner : " + Id + " cannot be found.";
            }

            response.setJsonPayload(updatedLearner);
            var result = caller->respond(response);
            if (result is error) {
                io:print("Error sending response", result);
            }
        } 
        else {
            response.statusCode = 400;
            response.setPayload("Invalid payload received");
            var result = caller->respond(response);
            if (result is error) {
                io:print("Error sending response", result);
            }
        }

    }

    resource function get learningMaterials() returns json {
        return getLearnerMaterials();
    }
}
